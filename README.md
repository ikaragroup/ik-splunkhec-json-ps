# ik-splunkhec-json-ps

Created by Peter Hauck for Ikara Group

### Description

Basic powershell to Splunk HEC.

This has been developed a local timing markers for push from client for start and end processes.

`Useage: ik-hecsplunk.ps1 [stage] [trans]`

[stage] - Stage or Step within script
[trans] - Transaction point - Usualy "start" and "end"
  Note: keep them the same to allow for transaction processing within Splunk

`Example:

ik-hecsplunk.ps1 Step1-Login start`

Data in Splunk:

{	[-]
 ipaddr: [	[-]
   192.168.1.158
   127.0.0.1
 ]
 localname: TESTPC1
 stage: Step1-Login
 time: 1525786181
 trans: start
}

`ik-hecsplunk.ps1 Step1-Login end`

Data in Splunk:

{	[-]
   ipaddr: [	[-]
     192.168.1.158
     127.0.0.1
   ]
   localname: IKPRES1
   stage: Step1-Login
   time: 1525786187
   trans: end
}


### Info

- HEC is setup as HTTP for ease of use - i.e. not secure
- Requires powershell 3.0

11:32 pm - 8 May 2018
