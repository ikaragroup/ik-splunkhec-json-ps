## Basic HTTP Event to Splunk
## Created by Peter Hauck - Ikara Group
## 2018
##
## Spluink Details - Set the following parameters.
##
$token = "9a787d7e-5799-42d4-ba3f-5c997e98a9a1"
$server = "192.168.1.25"
$port = 8088
##
## Shoul not need to edit below.
##
## Set Hostname
$localname = $env:COMPUTERNAME
## Get EPOCH Time
$local_epoch=[Math]::Floor([decimal](Get-Date(Get-Date).ToUniversalTime()-uformat "%s"))
## Get IP Address(es)
$ip=Get-NetIPAddress -AddressState Preferred -AddressFamily IPv4
#Build JSON object for compound transfer - inlcudes args for login and trans
$jsonMessage = (New-Object PSObject | Add-Member -PassThru NoteProperty localname $localname | Add-Member -PassThru NoteProperty ipaddr $ip.IPv4Address | Add-Member -PassThru NoteProperty stage $args[0] |  Add-Member -PassThru NoteProperty trans $args[1] | Add-Member -PassThru NoteProperty time $local_epoch) | ConvertTo-Json | Out-String
# Setup Transfer
$url = "http://${server}:$port/services/collector/event"
# Tidy things up a bit
$JsonString = $jsonMessage -replace "`n",' ' -replace "`r",' ' -replace ' ',''
# Build Event
$event = @{event = $JsonString} | ConvertTo-Json
# Build Header
$header = @{Authorization = "Splunk $token"}
# Send to Splunk
Invoke-RestMethod -Method Post -Uri $url -Headers $header -Body $event 
